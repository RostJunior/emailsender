<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Email-sender</title>
</head>
<body>
	<div id="main">

		<form action="sendMail" method="post" enctype="multipart/form-data">
			<p>
				Customer's name<br> <input type="text" title="enter name"
					name="customerName">
			</p>
			<p>
				Phone number<br> <input type="tel" title="enter numbers only"
					name="phoneNumber"
					onkeyup="this.value=this.value.replace(/[^\d]/,'')"
					placeholder="numbers only">
			</p>
			<p>
				E-mail<br> <input type="email" title="e-mail" name="email"
					placeholder="example@mail.com">
			</p>
			<p>
				Additional info<br>
				<textarea rows="5" cols="30" name="question"></textarea>
			</p>
			<p>
				<input type="file" title="Upload picture" name="pictureArray"
					accept="image/jpeg,image/jpg,image/gif,image/png"><br>
				<!-- This parameter can be set in servlet-context.xml -->
				<font size="2em" color="grey">Maximum image size 1Mb</font>
			</p>
			<button type="submit">Send</button>
			<input type="reset" value="Clear">
		</form>

	</div>
</body>
</html>
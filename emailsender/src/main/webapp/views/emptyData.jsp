<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Error</title>
</head>
<body>

	<h1>Unsuccessful!</h1>
	<p>You are trying to send empty data!</p>
	<a href="javascript:history.back()">return BACK</a>

</body>
</html>
package com.logos.sender.entity;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class MailSender {

	@Id
	@GeneratedValue
	private int id;

	@Column
	private String customerName;

	@Column
	private long phoneNumber;

	@Column
	private String email;

	@Column
	private String question;

	@Temporal(TemporalType.TIMESTAMP)
	@Column
	private Date date;

	private Byte[] pictureArray;

	public MailSender() {

	}

	public MailSender(String customerName, long phoneNumber, String email,
			String question) {
		this.customerName = customerName;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.question = question;
		date = new Timestamp(new Date().getTime());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public long getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public Byte[] getPicture() {
		return pictureArray;
	}

	public void setPicture(Byte[] picture) {
		this.pictureArray = picture;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "MailSender [id=" + id + ", customerName=" + customerName
				+ ", phoneNumber=" + phoneNumber + ", email=" + email
				+ ", question=" + question + ", picture="
				+ Arrays.toString(pictureArray) + "]";
	}

}

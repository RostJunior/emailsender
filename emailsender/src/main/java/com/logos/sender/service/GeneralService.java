package com.logos.sender.service;

import java.util.List;

public interface GeneralService<T, N extends Number> {

	@SuppressWarnings("unchecked")
	void create(T... type);

	@SuppressWarnings("unchecked")
	void update(T... type);

	List<T> getAll();

	@SuppressWarnings("unchecked")
	void delete(T... type);

	T findById(N id);
}

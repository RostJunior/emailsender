package com.logos.sender.dao;

import com.logos.sender.entity.MailSender;

public interface MailSenderDao {

	void save(MailSender mailSender);
}

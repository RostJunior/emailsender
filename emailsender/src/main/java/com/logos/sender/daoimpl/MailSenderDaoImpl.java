package com.logos.sender.daoimpl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.logos.sender.dao.MailSenderDao;
import com.logos.sender.entity.MailSender;

@Repository
public class MailSenderDaoImpl implements MailSenderDao {

	@PersistenceContext(unitName = "primary")
	private EntityManager em;

	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}

	@Transactional
	public void save(MailSender mailSender) {
		em.persist(mailSender);
	}

}

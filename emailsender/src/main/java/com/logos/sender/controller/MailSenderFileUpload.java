package com.logos.sender.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import org.springframework.web.multipart.MultipartFile;

public class MailSenderFileUpload {

	/**
	 * Method which allows to extract the name of the file from input field
	 * 
	 * @param file
	 * @return Name of the file
	 */
	public static String getFileName(MultipartFile file) {
		return file.getOriginalFilename();
	}

	/**
	 * Method for adding the file as attachment into mail
	 * 
	 * @param file
	 *            - our input file
	 * @return File to upload
	 */
	public static File fileUpload(MultipartFile file) {

		/**
		 * Getting file's name
		 */
		File serverFile = new File(getFileName(file));

		if (!file.isEmpty()) {
			try {
				/**
				 * Creating a byte-array for the uploaded file and store it in
				 * memory
				 */
				byte[] bytes = file.getBytes();

				/**
				 * Now collect the byte-array to file and send it directly to
				 * the method that creates the mail
				 */
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.flush();
				stream.close();

				System.out.println("Successful upload of the file - "
						+ getFileName(file));
			} catch (Exception e) {
				System.out.println("Failed to upload file - "
						+ getFileName(file) + " >>> " + e.getMessage());
			}
		} else {
			System.out.println("Failed to upload " + getFileName(file)
					+ " - empty file!");
			serverFile = null;
			return serverFile;
		}
		return serverFile;
	}
}

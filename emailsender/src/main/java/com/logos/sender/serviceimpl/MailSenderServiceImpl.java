package com.logos.sender.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logos.sender.dao.MailSenderDao;
import com.logos.sender.entity.MailSender;
import com.logos.sender.service.MailSenderService;

@Service
public class MailSenderServiceImpl implements MailSenderService {

	@Autowired
	private MailSenderDao dao;

	public void createTable(String customerName, long phoneNumber,
			String email, String question) {
		dao.save(new MailSender(customerName, phoneNumber, email, question));
	}

}
